# IMUplot.py
# By Michael Porritt & Grace Liu for BLUEsat
# To operate: run the MPU9250 measurement program on an arduino, then run this,
# this will internally open the serial port to initialise the arduino program 
# and will pass on and print the serial output. The serial output will also be 
# written to the text file given by f.
# The serial data should be of form [time] [ax] [ay] [az] [gx] ... [mz] - tab separated

import matplotlib.pyplot as plt
import numpy as np
import time
import math
import serial
import re


# Set Values:
time_limit = 600         # approximate runtime in seconds
delay = 0.05             # The rate of update of the graph/serial port
fname = 'IMUdata.txt'    # name of output file

serial_port = 'COM3'
baud_rate = 9600;


ser = serial.Serial(serial_port, baud_rate)
output_file = open(fname, 'w+')

t, ax, ay, az = [], [], [], []
gx, gy, gz = [], [], []
mx, my, mz = [], [], []


for i in range(math.floor(time_limit / delay)):
    line = ser.readline()
    if line != '':
        line = line.decode("utf-8") #ser.readline returns a binary, convert to string
        print(line, end='')
        output_file.write(line)


    nums = re.findall("[-+]?[.]?[\d]+(?:,\d\d\d)*[\.]?\d*(?:[eE][-+]?\d+)?", line)
    if len(nums) == 10:
        t.append(float(nums[0]))
        ax.append(float(nums[1]))
        ay.append(float(nums[2]))
        az.append(float(nums[3]))
        gx.append(float(nums[4]))
        gy.append(float(nums[5]))
        gz.append(float(nums[6]))
        mx.append(float(nums[7]))
        my.append(float(nums[8]))
        mz.append(float(nums[9]))


    if (i == 0):
        fig, axes = plt.subplots(3, sharex=True, sharey=True)
    axes[0].plot(t, ax, 'r', t, ay, 'g', t, az, 'b')
    axes[1].plot(t, gx, 'r', t, gy, 'g', t, gz, 'b')
    axes[2].plot(t, mx, 'r', t, my, 'g', t, mz, 'b')

    plt.pause(delay)    # updates the graph with new data

plt.show()
output_file.close()
